llm for Debian
--------------

llm as packaged for Debian should be generally useable out of the box
in itself. Plugin install functionality is limited by the way Python
environments are managed in Debian, which breaks the `llm install`
command, with the following error:

    error: externally-managed-environment

Since `llm install` basically just runs `pip install`, you can fix
this issue by calling pip directly, and, yes, breaking system
packages. For example, this command will install the `llm-cmd` plugin:

    pip3 install --break-system-packages llm-cmd

You can confirm this works by listing plugins with:

    llm plugins

Ideally, all plugins would be packaged in Debian, but there are a lot
of them right now, and many have their own dependency chains with
packages still missing from Debian.

 -- Antoine Beaupré <anarcat@debian.org>, Wed, 16 Oct 2024 21:40:49 -0400
